<?php 
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

require './controller/Names.php';

Class Main
{
    var $projectFolder;
    public function main()
    {
        try {
            $this->projectFolder = '';

            $uri = parse_url($_SERVER['REQUEST_URI']);

            $parameters = explode('/', $uri['path']);
            
            $start = ($this->projectFolder != '') ? $this->_constructArguments($parameters) : 1;

            if($start != -1){
               
                $controller_name = ! empty ($parameters[$start]) ? $parameters[$start] : 'names';

                if( isset ($parameters[$start + 1]) AND $parameters[$start + 1]) {
                    $function_name = $parameters[$start + 1];
                } else {
                    $function_name = 'pages';
                }

                $start += 2;
        
                $args = [];
                
                if (isset($parameters[$start])){
                    for(;$start < count($parameters); $start++){
                        array_push($args, $parameters[$start]);
                    }
                }
                call_user_func_array(array(new $controller_name, $function_name), $args);
        
            }else{
                echo "404 not found";
            }
            
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private function _constructArguments($uri)
    {
		foreach ($uri as $key => $value){
			if($value == $this->projectFolder){
				if($key == count($uri) - 1 ) return -1;
				return $key+1;
			}
		}
    }
}

$main = new Main();
