<!DOCTYPE html>
<html lang="en">
<head>
    <title>Names</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= $baseUrl ?>resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= $baseUrl ?>resources/css/base.css">
    <script src="<?= $baseUrl ?>resources/js/jquery-3.4.1.min.js"></script>
    <script src="<?= $baseUrl ?>resources/js/popper.min.js"></script>
    <script src="<?= $baseUrl ?>resources/js/bootstrap.min.js"></script>
</head>
    <body class="masthead pb-5">
        <header class="masthead-page">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-8 col-sm-12 py-2">
                        <h2 class="mb-2 text-white">List of Names</h2>
                    </div>
                    <?php if($names):?>
                    <div class="col-lg-4 col-sm-12">
                        <button onclick="Names.modalOpen('','')" type="button" 
                        class="btn btn-info float-right" data-toggle="modal">Add Name</button>
                    </div>
                    <?php endif;?>
                </div>
            </div>
        </header>
        <div class="container mt-2">
            <input type="hidden" name="baseUrl" value="<?= $baseUrl ?>">
            <div class="row">
                <?php if($names):?>
                <div class="col-sm-12">
                    <ul class="list-group">
                        <?php foreach($names as $name):?>
                            <li class="list-group-item">
                                <?= $name['name'] ?>
                                <button type="button" 
                                    class="badge badge-danger badge-pill float-right"
                                    onclick="Names.deleteRecord('<?= $name['id'] ?>')">
                                    DELETE</button>
                                <button type="button" 
                                    class="badge badge-secondary badge-pill float-right"
                                    data-toggle="modal" 
                                    onclick="Names.modalOpen('<?= $name['id'] ?>',
                                        '<?= $name['name'] ?>')">
                                    EDIT</button>
                              </li>
                        <?php endforeach;?>
                    </ul>
                </div>
                <?php else: ?>
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-center">
                            <span style='font-size:100px;'>&#128533;</span>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title d-flex justify-content-center">
                                Oops! No available data yet.</h5>
                            <p class="card-text d-flex justify-content-center">
                                You seem to be using the system for the first time 
                                or have removed everything on it.</p>
                            <div class="d-flex justify-content-center">
                                <button onclick="Names.modalOpen('','')" type="button" 
                                    class="btn btn-info float-right" 
                                    data-toggle="modal">Create One Now
                                </button>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <?php endif;?>
                <div class="col-sm-12 pt-2 <?= ($pagesCount == 0) ? 'd-none':'' ?>">
                    <nav aria-label="...">
                        <ul class="pagination justify-content-end">
                            <li class="page-item <?= ($activePage == 1) ? 'disabled' : '' ?>">
                                <a class="page-link" href="javascript:;" 
                                    data-page = "<?= $activePage ?>" 
                                    data-action="prev">Previous
                                </a>
                            </li>
                            <?php for($x = 1; $x <= $pagesCount; $x++):?>
                            <li class="page-item <?= ($x == $activePage) ? 'active':'' ?>">
                                <a class="page-link" href="javascript:;" 
                                    data-page = "<?= $x ?>" 
                                    data-action="direct"><?= $x ?>
                                </a>
                            </li>
                            <?php endfor;?>
                            <li class="page-item <?= ($activePage == $pagesCount) ? 
                                'disabled' : '' ?>">
                                <a class="page-link" href="javascript:;" 
                                    data-page = "<?= $activePage ?>" data-action="next">
                                    Next
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="modal" id="nameModal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form id="nameForm">
                                <input type="hidden" name="id">
                                <div class="modal-header">
                                    <h4 class="modal-title">Add Name</h4>
                                    <button type="button" class="close" data-dismiss="modal">
                                        &times;
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="email">Name </label>
                                        <input name="name" type="text" maxlength="100" 
                                            class="form-control" placeholder="Enter name" 
                                            id="name" required>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" 
                                        data-dismiss="modal">
                                        Close
                                    </button>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="alertModal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body text-white">
                                <button type="button" class="float-right close" 
                                    data-dismiss="modal">
                                    &times;
                                </button>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="confirmModal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <!-- dialog body -->
                            <div class="modal-body">
                                <button type="button" class="close" 
                                    data-dismiss="modal">
                                    &times;
                                </button>
                                Are you sure you want to delete this record?
                            </div>
                            <!-- dialog buttons -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" 
                                    data-dismiss="modal">
                                    Cancel
                                </button>
                                <button id="confirmDeleteOK" type="button" 
                                    class="btn btn-primary" data-dismiss="modal">
                                    OK
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="<?= $baseUrl ?>resources/js/names.js"></script>
    </body>
</html>


