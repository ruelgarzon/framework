<?php 

require_once __DIR__.'/../core/Model.php';

class NamesModel extends Model
{

    public $name;
    public $id;
    public $limitStart = 0;
    public $limitEnd = 10;
    public function __construct(){
        parent::__construct();
    }

    public function getNames()
    {
        $start = $this->limitStart;
        $end = $this->limitEnd;
        $query = "SELECT id,name FROM names LIMIT $start, $end";
        $result = $this->query($query, [], TRUE);
        return $result;
    }
    public function getTotalCnt()
    {
        $query = 'SELECT count(id) cnt FROM names';
        $result = $this->query($query, [], TRUE, FALSE);
        return $result['cnt'];
    }

    public function setName(string $newName) 
    { 
        $this->name = $newName;  
    }

    public function getName() 
    {
        return $this->name;
    }

    public function setId(int $newId) 
    { 
        $this->id = $newId;  
    }

    public function getId() 
    { 
        return $this->id;
    }

    public function insertName()
    {
        $query = 'INSERT INTO names (name) values (?)';
        $val[] = $this->name;
        return $this->id = $this->query($query, $val, FALSE, FALSE, TRUE);
    }

    public function updateName()
    {
        $query = 'UPDATE names SET name = ? WHERE id = ?';
        $val[] = $this->name;
        $val[] = $this->id;
        $this->query($query,$val,FALSE);
    }

    public function deleteName()
    {
        $query = 'DELETE FROm names WHERE id = ?';
        $val[] = $this->id;
        $this->query($query,$val,FALSE);
    }
}