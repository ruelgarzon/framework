var Names = function () 
{
    var baseUrl = $('input[name=baseUrl]').val();
    var init = function () 
    {
        $('#nameForm').off( "submit.update").on( "submit.update",function(e) {
            e.preventDefault();
            var data = $(this).serialize();
            $.post(baseUrl + 'names/processRecord', data, function(result)
            {
                notifMsg(result.status, result.msg);
                if(result.status) {
                    $('#nameModal').modal('hide');
                    setTimeout(function(){ window.location.reload(); }, 1000);
                }
            }, 'json');
        });
    }

    var deleteRecord = function (id) 
    {
        $('#confirmModal').modal('show');
        $('#confirmDeleteOK').off( "click").on( "click",function(e) {
            var data = {'id':id};
            $.post(baseUrl + 'names/deleteRecord', data, function(result)
            {
                notifMsg(result.status, result.msg);
                if(result.status) {
                    setTimeout(function(){ window.location.reload(); }, 1000);
                }
            }, 'json');
        });
    }
    var modalOpen = function (id, name) 
    {
        $('input[name=id]').val(id);
        $('input[name=name]').val(name);

        if(id == ''){
            $('#nameModal .modal-title').html('Add Name');
        }
        else{
            $('#nameModal .modal-title').html('Edit Name');
        }
        $('#nameModal').modal('show');
    }

    var notifMsg = function (status, msg) 
    {
        $('#alertModal .modal-body').removeClass('bg-success');
        $('#alertModal .modal-body').removeClass('bg-danger');
        var type = (status == true) ? 'bg-success' : 'bg-danger';
        $('#alertModal .modal-body').addClass(type);
        $('#alertModal .modal-body span').html(msg);
        $('#alertModal').modal('show');
    }
    var pageInit = function () 
    {
        $('ul.pagination li.page-item a').off( "click").on( "click",function(e) {
            var id = $(this).data('page');
            var action = $(this).data('action');
            if(action == "prev"){
                id -= 1;
            }else if(action == "next"){
                id += 1;
            }
            window.location.href = baseUrl + 'names/pages/' + id;
        });
    }
    
    return {
        
        init : function () {
            init();
            pageInit();
        },
        modalOpen : function (id, name) {
            modalOpen(id, name);
        },
        deleteRecord : function (id) {
            deleteRecord(id);
        }

    }
    
}();

Names.init();