<?php 

require_once __DIR__.'/../model/NamesModel.php';
require_once __DIR__.'/../core/Controller.php';

class Names extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function pages(int $pageNumber = 1) 
    {
        try {
            $model = new NamesModel();
            $dataLimit = $model->limitEnd;

            $model->limitStart = $dataLimit * ($pageNumber - 1);
            $names = $model->getNames();
            $this->_encryptIds($names);

    	    $data = ['names' => $names];

            $totalCnt = $model->getTotalCnt();
            $pagesCount = ceil($totalCnt / $dataLimit);
            $data['pagesCount'] = $pagesCount;
            $data['activePage'] = $pageNumber;

    	    $this->loadView('names',$data);
        } catch (PDOException $e) {
            $msg = $e->getMessage();
        } catch (Exception $e) {
            $msg = $e->getMessage();
        }
    }

    public function processRecord() 
    {
        try {
            $params = $_POST;
            $msg = '';
            $status = false;
            $id = $params['id'];

            if(empty($params['name']))
                throw new Exception('Name is required.');

            $newName = $params['name'];

            if($id) {
                $id = $this->encrypt_decrypt($id, FALSE);
                $msg = $this->_nameUpdate($newName, $id);
            } else {
                $msg = $this->_nameInsert($newName);
            }
            $status = true;
                

        } catch (PDOException $e) {
            $msg = $e->getMessage();
        } catch (Exception $e) {
            $msg = $e->getMessage();
        }

        $data = ['status' => $status,
                'msg' => $msg
        ];
        echo json_encode($data);
    }


    private function _nameInsert(string $name):string
    {
        $model = new NamesModel();
        $model->setName($name);
        $model->insertName();
        return 'Record was successfully inserted.';
    }

    private function _nameUpdate(string $name, int $id):string 
    {
        $model = new NamesModel();
        $model->setName($name);
        $model->setId($id);
        $model->updateName();
        return 'Record was successfully updated.';
    }

    public function deleteRecord() 
    {

        try {

            $params = $_POST;
            $msg = '';
            $status = false;
            $id = $params['id'];
            if(empty($id))
                throw new Exception('Invalid action.');

            $id = $this->encrypt_decrypt($id, FALSE);

            $msg = $this->_nameDelete($id);
            $status = true;

        } catch (PDOException $e) {
            $msg = $e->getMessage();
        } catch (Exception $e) {
            $msg = $e->getMessage();
        }

        $data = ['status' => $status,
                'msg' => $msg
        ];
        echo json_encode($data);
    }

    private function _nameDelete(int $id):string 
    {
        $model = new NamesModel();
        $model->setId($id);
        $model->deleteName();
        return 'Record was successfully deteled.';
    }
     
    private function _encryptIds(array &$names)
    {
        foreach ($names as $key => $value) {
            $names[$key]['id'] = $this->encrypt_decrypt($value['id'], TRUE);
        }
    }
}