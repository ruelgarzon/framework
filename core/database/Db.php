<?php 


class Db
{
	private $dbConfig;
	
	public function __construct()
	{
		$dbParams = [];
		require_once __DIR__.'/../../config/database.php';
		$this->dbConfig = $dbParams;
	}

	public function getConnection()
	{
	    try {
	    	$serverName = $this->dbConfig['serverName'];
	    	$dbName = $this->dbConfig['dbName'];

            $conn = new PDO("mysql:host=$serverName;dbname=$dbName", $this->dbConfig['userName'], $this->dbConfig['password']);
    
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $conn;
        }
        catch(PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }

}