<?php 


class Controller 
{
	public $baseUrl;
	public function __construct()
	{
		$this->baseUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') ? 
            'https' : 'http'; 
        $this->baseUrl .= '://';
        $this->baseUrl .= $_SERVER['HTTP_HOST'];
        $this->baseUrl .= chop($_SERVER['PHP_SELF'],'/index.php').'/';
	}

	public function loadView($view, $args)
	{
		$args['baseUrl'] = $this->baseUrl;
		foreach ($args as $vname => $vvalue) {
			
			$$vname = $vvalue;
		}
		require_once __DIR__.'/../view/'.$view.'.php';
	}
	
	public function encrypt_decrypt(string $cryptedId, bool $encrypt = TRUE) 
    {
        $encryptMethod = "AES-256-CBC";
        $secretKey = 'ORMfram3w0rkS3curityKEY';
        $secretIv = 'ORMfram3w0rkS3curityIV';
        // hash
        $key = hash('sha256', $secretKey);    
        // iv - encrypt method AES-256-CBC expects 16 bytes 
        $iv = substr(hash('sha256', $secretIv), 0, 16);
        if ($encrypt) {
            $output = openssl_encrypt($cryptedId, $encryptMethod, $key, 0, $iv);
            $output = base64_encode($output);
        } else {
            $output = openssl_decrypt(base64_decode($cryptedId), $encryptMethod, $key, 0, $iv);
        }
        return $output;
    }
}