<?php

require_once __DIR__.'/database/Db.php';

class Model{

    public function __construct()
    {
        $this->db = new Db();
    }
    /*
    *  parameters
    *  $query - sql statement
    *  $val - prepared statements parameters 
    *  $select - determines whether the sql query is a select statement
    *  $multiple - if the sql query is a select statement,set this parameter will determine whether to return single or multiple record
    *  $return_inserted_id - if the sql query is an insert statement,this parameter will return the auto-generated id of the inserted record
    */
    public function query(string $query, array $val = NULL, bool $select = TRUE, 
        bool $multiple = TRUE, bool $return_inserted_id = FALSE)
    {   
        try {       
            $this->connection = $this->db->getConnection();                    
            $stmt = $this->connection->prepare($query);
            $stmt->execute($val);
            
            if($select) {
                if($multiple === TRUE) {
                    return $stmt->fetchAll(PDO::FETCH_ASSOC);
                } else {
                    return $stmt->fetch(PDO::FETCH_ASSOC);
                }
            } else {
                if( $return_inserted_id ) {
                    $last_inserted_id = $this->connection->lastInsertId();

                    return $last_inserted_id;
                } else {
                    return $stmt;
                }
            }
        } catch (PDOException $e) {
            throw $e;
        }       
    }
}

